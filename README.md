# Tasky - Aplikacija za pohranu obaveza #

## Opis aplikacije ##

Pri otvaranju aplikacije, prikazuju se pohranjene obaveze (ako ih ima), a na dnu se nalaze dva gumba, jedan za dodavanje novih obaveza, a druga za dodavanje i brisanje kategorija obaveza. Postavljanje pozicija gumba nađeno je na [1]. Klikom na gumb za dodavanje novih obaveza, korisniku je ponuđen unos naslova i opisa obaveza, odabir prioriteta (niski, srednji i visoki prioritet) i kategorije obaveza. Klikom na gum "ADD TASK" dodaje se novi zadatak i vraća na početni zaslon gdje se na dnu popisa prikazuje dodani zadatak te je obojan bojom ovisno o prioritetu zadatka (zelena za niski, žuta za srednji i crvena za visoki prioritet zadatka). Zadatak je moguće obrisati dugim klikom na njega čime se prikazuje poruka upozorenja koja pita želi li se obrisati zadatak gdje korisnik može odabrati "OK" ili "CANCEL". Klikom na gumb za kategorije, korisniku može unijeti novu kategoriju koju potvrđuje klikom na gumb "ADD CATEGORY" ili pregledati već unešene kategorije koje su prikazane ispod i na dugi klik ih obrisati.

## Rješenja i problemi pri stvaranju aplikacije ##

Na početni zaslon dodan je "ListView" koji čita podatke iz tablice "tasks" koji su napravljeni po uzoru na LV3. Klikom na gumb "NEW TASK" otvara se novi activity u kojemu su dana dva "EditText" elementa za unos naslova i opisa obaveze te dva "spinner" elementa, jedan je za prioritet i polje stringova je zadano u xml datoteci, a drugi čita podatke iz baze i posebne tablice "table_category" koja ima jedan redak, nazive kategorija. Postavljena je mogućnost ako nema niti jedne kategorije (niti jednog unosa u "spinner" elementu) naziv kategorije bude prazan ([6]). Klikom na gumb "ADD TASK" podaci se spremaju u bazu i vraća se na glavni activity gdje je na dnu liste prikazana unešena obaveza jer "ListView" prikazuje sve podatke iz tablice "tasks" pomoću select naredbe koja je zapisana u "TaskDBHelper". Klikom na gumb "CATEGORY" otvara se novi activity gdje je "EditText" za unos nove kategorije, a ispod njega je "ListView" koji prikazuje sve unešene kategorije. "ListView" se puni iz tablice "table_category" gdje se nalaze samo unešene kategorije. Naknadnim dodavanjem tablice "table_category", aplikacija se srušila jer nije nađena tražena tablica, no rješenje tog problema je nađeno na [2]. Dugim klikom na neki od elemenata "ListView-a" (kategorije ili obaveze) pojavljuje se poruka upozorenja koja pita želi li se obrisati kategorija/obaveza. Pravljenje poruke upozorenja nađeno je na [3]. Ako korisnik klikne na "CANCEL", program ništa ne radi, ali ako klikne na "OK" dohvaća se objekt iz "ListView-a" (rješenje za to nađeno na [4]), obriše se taj objekt iz baze (rješenje nađeno na [5]) i iz "ListView-a" pomoću adaptera (napavljeno po uzoru na LV3). Ikona aplikacije nađena je pod [7].
Aplikacija radi u "portret" načinu rada i "landscape" načinu rada koji je omogućen "ScrollView-om".

## Testiranje ##

Testiranje je obavljeno na dva uređaja, prvi je 5.5'' FHD mobilni uređaj (detalji na [8]) i screenshot-ovi su u mapi "Screenshots_DZ3" pod nazivom "test1_X", a drugi je 5'' HD mobilni uređaj (detalji na [9]) čiji su screenshot-ovi spremljeni pod nazivom "test2_X".

### Linkovi na vanjske resurse ###

[1] http://stackoverflow.com/questions/31696384/android-studio-put-two-buttons-side-by-side-in-relativelayout

[2] http://stackoverflow.com/questions/26724637/activeandroid-sqlite-exception-no-such-table

[3] http://stackoverflow.com/questions/2115758/how-do-i-display-an-alert-dialog-on-android

[4] http://stackoverflow.com/questions/7073577/how-to-get-object-from-listview-in-setonitemclicklistener-in-android

[5] http://stackoverflow.com/questions/15027474/android-sqlite-deleting-a-specific-row-from-database

[6] http://stackoverflow.com/questions/29891237/checking-if-spinner-is-selected-and-having-null-value-in-android

[7] https://www.google.hr/imgres?imgurl=http%3A%2%2Fcdn.appstorm.net%2Fmac.appstorm.net%2Fmac%2Ffiles%2F2013%2F06%2Ftaskdeck_icon.png&imgrefurl=http%3A%2F%2Fmac.appstorm.net%2Freviews%2Fproductivity-review%2Ftaskdeck-a-simple-keyboard-controlled-menubar-todo-list%2F&docid=4_DuOATbTFJEQM&tbnid=XhaGteAWMQIItM%3A&vet=10ahUKEwje04_LzaPTAhWEPFAKHcVfBokQMwgyKBIwEg..i&w=200&h=200&client=firefox-b&bih=770&biw=1536&q=to%20do%20list%20icon%20png&ved=0ahUKEwje04_LzaPTAhWEPFAKHcVfBokQMwgyKBIwEg&iact=mrc&uact=8

[8] http://www.devicespecifications.com/en/model/3a353426

[9] http://www.gsmarena.com/lenovo_c2_power-8368.php