package com.example.gabrijela.dz3;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class AddNewCategory extends AppCompatActivity implements View.OnClickListener {

    EditText etNewCategory;
    Button bAddNewCategory;
    ListView lvCategories;
    CategoryAdapter mCategoryAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_category);
        this.setUpUI();
    }

    private void setUpUI() {
        this.bAddNewCategory = (Button) findViewById(R.id.bAddNewCategory);
        this.etNewCategory = (EditText) findViewById(R.id.etNewCategory);
        this.lvCategories = (ListView) findViewById(R.id.lvCategories);
        bAddNewCategory.setOnClickListener(this);
        ArrayList<String> category = this.loadCategory();
        CategoryAdapter categoryAdapter = new CategoryAdapter(category);
        this.lvCategories.setAdapter(categoryAdapter);
        this.mCategoryAdapter = new CategoryAdapter(this.loadCategory());
        this.lvCategories.setAdapter(this.mCategoryAdapter);
        this.lvCategories.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final String category = (String) parent.getAdapter().getItem(position);
                new AlertDialog.Builder(AddNewCategory.this)
                        .setTitle("Delete category")
                        .setMessage("Are you sure you want to delete this category?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                TaskDBHelper.getInstance(getApplicationContext()).deleteCategory(category);
                                mCategoryAdapter.deleteAt(position);
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            }
        });
    }

    private ArrayList<String> loadCategory() {
        return TaskDBHelper.getInstance(this).getAllCategories();
    }

    @Override
    public void onClick(View v) {
        Intent explicitIntent = new Intent();
        String newCategory = etNewCategory.getText().toString();
        if (newCategory.equals("")) newCategory = "";
        TaskDBHelper.getInstance(getApplicationContext()).insertCategory(newCategory);
        explicitIntent.setClass(getApplicationContext(), MainActivity.class);
        startActivity(explicitIntent);
    }
}
