package com.example.gabrijela.dz3;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Gabrijela on 13.4.2017..
 */

public class CategoryAdapter extends BaseAdapter {
    private ArrayList<String> mCategory;

    public CategoryAdapter(ArrayList<String> category) {
        mCategory = category;
    }

    @Override
    public int getCount() {
        return this.mCategory.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mCategory.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        CategoryAdapter.ViewHolder categoryViewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_category, parent, false);
            categoryViewHolder = new CategoryAdapter.ViewHolder(convertView);
            convertView.setTag(categoryViewHolder);
        } else {
            categoryViewHolder = (CategoryAdapter.ViewHolder) convertView.getTag();
        }
        String category = this.mCategory.get(position);
        categoryViewHolder.tvCategoryEl.setText(category);
        return convertView;
    }

    public void deleteAt(int position) {
        this.mCategory.remove(position);
        this.notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView tvCategoryEl;
        public ViewHolder(View taskView) {
            tvCategoryEl = (TextView) taskView.findViewById(R.id.tvCategoryLE);

        }
    }
}
