package com.example.gabrijela.dz3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;


/**
 * Created by Gabrijela on 11.4.2017..
 */

public class TaskDBHelper extends SQLiteOpenHelper {

    private static TaskDBHelper mTaskDBHelper = null;

    TaskDBHelper(Context context) {
        super(context.getApplicationContext(), Schema.DATABASE_NAME, null, Schema.SCHEMA_VERSION);
    }

    public static synchronized TaskDBHelper getInstance(Context context) {
        if (mTaskDBHelper == null) mTaskDBHelper = new TaskDBHelper(context);
        return mTaskDBHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TASKS);
        db.execSQL(CREATE_TABLE_CATEGORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_TABLE_TASKS);
        db.execSQL(DROP_TABLE_CATEGORY);
        this.onCreate(db);
    }

    //SQL statements
    static final String CREATE_TABLE_TASKS = "CREATE TABLE " + Schema.TABLE_TASKS + "(" + Schema.TITLE + " TEXT, " + Schema.DESCRIPTION + " TEXT, " + Schema.CATEGORY + " TEXT, " + Schema.STATUS + " TEXT);";
    static final String CREATE_TABLE_CATEGORY = "CREATE TABLE " + SchemaCategory.TABLE_CATEGORY + "(" + SchemaCategory.CATEROGRY_ROW + " TEXT);";
    static final String DROP_TABLE_TASKS = "DROP TABLE IF EXISTS " + Schema.TABLE_TASKS;
    static final String DROP_TABLE_CATEGORY = "DROP TABLE IF EXISTS " + SchemaCategory.TABLE_CATEGORY;
    static final String SELECT_ALL_TASKS = "SELECT * FROM " + Schema.TABLE_TASKS;
    static final String SELECT_ALL_CATEGORIES = "SELECT * FROM " + SchemaCategory.TABLE_CATEGORY;

    public void insertTask(Task task) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Schema.TITLE, task.getTitle());
        contentValues.put(Schema.DESCRIPTION, task.getDescription());
        contentValues.put(Schema.CATEGORY, task.getCategory());
        contentValues.put(Schema.STATUS, task.getStatus());
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        writableDatabase.insert(Schema.TABLE_TASKS, Schema.TITLE, contentValues);
        writableDatabase.close();
    }

    public void insertCategory(String category) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(SchemaCategory.CATEROGRY_ROW, category);
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        writableDatabase.insert(SchemaCategory.TABLE_CATEGORY, SchemaCategory.CATEROGRY_ROW, contentValues);
        writableDatabase.close();
    }

    public void deleteTask(Task task) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        writableDatabase.delete(Schema.TABLE_TASKS, Schema.TITLE + " = ?", new String[]{task.getTitle()});
        writableDatabase.close();
    }

    public void deleteCategory(String category) {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        writableDatabase.delete(SchemaCategory.TABLE_CATEGORY, SchemaCategory.CATEROGRY_ROW + " = ?", new String[]{category});
        writableDatabase.close();
    }

    public ArrayList<Task> getAllTasks() {
        SQLiteDatabase writeableDatabase = this.getWritableDatabase();
        Cursor taskCursor = writeableDatabase.rawQuery(SELECT_ALL_TASKS, null);
        ArrayList<Task> tasks = new ArrayList<>();
        if (taskCursor.moveToFirst()) {
            do {
                String title = taskCursor.getString(0);
                String description = taskCursor.getString(1);
                String category = taskCursor.getString(2);
                String status = taskCursor.getString(3);
                tasks.add(new Task(title, description, category, status));
            } while (taskCursor.moveToNext());
        }
        taskCursor.close();
        writeableDatabase.close();
        return tasks;
    }

    public ArrayList<String> getAllCategories() {
        SQLiteDatabase writableDatabase = this.getWritableDatabase();
        Cursor categoryCursor = writableDatabase.rawQuery(SELECT_ALL_CATEGORIES, null);
        ArrayList<String> categories = new ArrayList<>();
        if (categoryCursor.moveToFirst()) {
            do {
                categories.add(categoryCursor.getString(0));
            } while (categoryCursor.moveToNext());
        }
        categoryCursor.close();
        writableDatabase.close();
        return categories;
    }

    public static class Schema {
        private static final int SCHEMA_VERSION = 5;
        private static final String DATABASE_NAME = "task.db";
        static final String TABLE_TASKS = "tasks";
        static final String TITLE = "title";
        static final String DESCRIPTION = "description";
        static final String CATEGORY = "category";
        static final String STATUS = "status";
    }

    public static class SchemaCategory {
        static final String TABLE_CATEGORY = "table_category";
        static final String CATEROGRY_ROW = "category_name";
    }
}
