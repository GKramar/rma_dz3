package com.example.gabrijela.dz3;

/**
 * Created by Gabrijela on 9.4.2017..
 */

public class Task {
    private String title, description, category, status;

    public Task(String title, String description, String category, String status) {
        this.title = title;
        this.description = description;
        this.status = status;
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
