package com.example.gabrijela.dz3;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    ListView lvTaskList;
    TaskAdapter mTaskAdapter;
    Button bTask;
    Button bCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setUpUI();
    }

    private void setUpUI() {
        this.lvTaskList = (ListView) findViewById(R.id.lvTask);
        this.bCategory = (Button) findViewById(R.id.bNewCategory);
        this.bTask = (Button) findViewById(R.id.bNewTask);
        ArrayList<Task> tasks = this.loadTasks();
        TaskAdapter taskAdapter = new TaskAdapter(tasks);
        this.lvTaskList.setAdapter(taskAdapter);
        bTask.setOnClickListener(this);
        bCategory.setOnClickListener(this);
        this.mTaskAdapter = new TaskAdapter(this.loadTasks());
        this.lvTaskList.setAdapter(this.mTaskAdapter);
        this.lvTaskList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                final Task task = (Task) parent.getAdapter().getItem(position);
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Delete task")
                        .setMessage("Are you sure you want to delete this task?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                TaskDBHelper.getInstance(getApplicationContext()).deleteTask(task);
                                mTaskAdapter.deleteAt(position);
                                setUpUI();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                // do nothing
                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
                return true;
            }
        });
    }

    private ArrayList<Task> loadTasks() {
        return TaskDBHelper.getInstance(this).getAllTasks();
    }

    @Override
    public void onClick(View v) {
        Intent explicitIntent = new Intent();
        switch (v.getId()) {
            case (R.id.bNewCategory):
                explicitIntent.setClass(getApplicationContext(), AddNewCategory.class);
                break;
            case (R.id.bNewTask):
                explicitIntent.setClass(getApplicationContext(), AddNewTask.class);
                break;
        }
        this.startActivity(explicitIntent);

    }
}
