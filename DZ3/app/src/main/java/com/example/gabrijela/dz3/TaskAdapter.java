package com.example.gabrijela.dz3;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Gabrijela on 9.4.2017..
 */

public class TaskAdapter extends BaseAdapter {

    private ArrayList<Task> mTask;

    public TaskAdapter(ArrayList<Task> task) {
        mTask = task;
    }

    @Override
    public int getCount() {
        return this.mTask.size();
    }

    @Override
    public Object getItem(int position) {
        return this.mTask.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder taskViewHolder;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.item_task, parent, false);
            taskViewHolder = new ViewHolder(convertView);
            convertView.setTag(taskViewHolder);
        } else {
            taskViewHolder = (ViewHolder) convertView.getTag();
        }
        Task task = this.mTask.get(position);
        taskViewHolder.tvTaskTitle.setText(task.getTitle());
        taskViewHolder.tvTaskDescription.setText(task.getDescription());
        taskViewHolder.tvTaskCategory.setText(task.getCategory());
        switch (task.getStatus()) {
            case ("High"):
                convertView.setBackgroundResource(R.drawable.priority_high);
                break;
            case ("Medium"):
                convertView.setBackgroundResource(R.drawable.priority_medium);
                break;
            case ("Low"):
                convertView.setBackgroundResource(R.drawable.priority_low);
                break;
        }
        return convertView;
    }

    public void deleteAt(int position) {
        this.mTask.remove(position);
        this.notifyDataSetChanged();
    }

    public static class ViewHolder {
        public TextView tvTaskTitle;
        public TextView tvTaskDescription;
        public TextView tvTaskCategory;

        public ViewHolder(View taskView) {
            tvTaskTitle = (TextView) taskView.findViewById(R.id.tvTitle);
            tvTaskDescription = (TextView) taskView.findViewById(R.id.tvDescription);
            tvTaskCategory = (TextView) taskView.findViewById(R.id.tvCategory);


        }
    }

}
