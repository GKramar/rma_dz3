package com.example.gabrijela.dz3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class AddNewTask extends AppCompatActivity implements View.OnClickListener {

    EditText etTitle, etDescription;
    Button bAddNewTask;
    Spinner spStatus, spCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_task);
        this.spStatus = (Spinner) findViewById(R.id.spStatus);
        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter.createFromResource(this, R.array.spStatus, android.R.layout.simple_spinner_item);
        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spStatus.setAdapter(staticAdapter);
        this.spCategory = (Spinner) findViewById(R.id.spCategory);
        loadSpinnerData();
        this.setUpUI();
    }

    private void loadSpinnerData() {
        TaskDBHelper db = new TaskDBHelper(getApplicationContext());
        ArrayList<String> categories = db.getAllCategories();
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spCategory.setAdapter(dataAdapter);
    }

    private void setUpUI() {
        this.bAddNewTask = (Button) findViewById(R.id.bAddNewTask);
        this.etTitle = (EditText) findViewById(R.id.etNewTaskTitle);
        this.etDescription = (EditText) findViewById(R.id.etNewTaskDescription);
        bAddNewTask.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent explicitIntent = new Intent();
        String title = etTitle.getText().toString();
        String description = etDescription.getText().toString();
        String category;
        if (spCategory != null && spCategory.getSelectedItem() != null)
            category = spCategory.getSelectedItem().toString();
        else category = "";
        String status = spStatus.getSelectedItem().toString();
        Task task = new Task(title, description, category, status);
        TaskDBHelper.getInstance(getApplicationContext()).insertTask(task);
        explicitIntent.setClass(getApplicationContext(), MainActivity.class);
        startActivity(explicitIntent);
    }
}
